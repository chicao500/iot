import sys
import requests
from time import sleep
import Adafruit_DHT as dht
import datetime
# Aqui coloque o TOKEN gerado pelo CANAL do ThingSeak
myAPI = 'P59V3O8NRX27FZ8W'
# URL do ThingSpeak concatenando com o Token gerado pelo canal
baseURL = 'https://api.thingspeak.com/update?api_key=%s' % myAPI 
def DHT11_data():
# Lendo pelo DHT11 os valores de humidade e temperatura
    humi, temp = dht.read_retry(dht.DHT11, 25)
    return  temp, humi

while True:
    try:
        humi, temp = DHT11_data()
            #verificando se a leitura de humidade e temperatura é valida
        if isinstance(humi, float) and isinstance(temp, float):
            # Formatando para decimal
            humi = '%.2f' % humi         
            temp = '%.2f' % temp
            now = datetime.datetime.now()
            data = now.strftime('%d/%m/%Y %H:%M:%S')
            
            # Enviando data para o ThingSpeak
            conn = requests.get(baseURL + '&field1=%s&field2=%s&field3=%s' % (temp, humi,data))
            #print conn.read()
            print('temperatura: ' + temp)
            print('umidade: ' + humi)
            print(data)
            print('---')
            # Encerrando a conexão
            conn.close()
        else:
            print ('Error')
            # intervalo de 1 segundo para ler o sensor novamente
            sleep(1)
    except:
        print('Erro!')
        break