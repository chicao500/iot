import RPi.GPIO as GPIO
import time
from picamera import PiCamera

GPIO.setmode(GPIO.BOARD) 
GPIO.setup(11, GPIO.IN) 

def tirarFoto():
    camera = PiCamera()
    camera.resolution = (1280, 720)
    camera.start_preview()
    time.sleep(4)
    camera.capture('output23.jpg')
    camera.stop_preview()



while (True):
    if(GPIO.input(11) == 1):
       print("Algo foi detectado! " + str(GPIO.input(11)))
       tirarFoto()
       break
    else:
       print('Nada se mexendo!'+ str(GPIO.input(11)))      
    time.sleep(0.2)
